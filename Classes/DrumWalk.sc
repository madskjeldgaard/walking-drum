DrumWalk{
    classvar package;

    *setup{
        load(this.path() +/+ "patches" +/+ "setup.scd")
    }

    *start{
        load(this.path() +/+ "patches" +/+ "start.scd")
    }

    *stop{
        load(this.path() +/+ "patches" +/+ "stop.scd")
    }

    *thisPackage{
        package = Quarks.findClassPackage(this)
        ^package
    }

    *thisPackageName{
        ^this.thisPackage().name
    }

    *path{
        ^this.thisPackage.localPath();
    }
}
