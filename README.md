# Walking drum

This is a SuperCollider patch used by Mads Kjeldgaard for live performances. 

It receives OSC messages from a phone running TouchOSC and uses those to trigger a pattern that plays percussion sounds.

It may be used with poetry or very slow body movements.

